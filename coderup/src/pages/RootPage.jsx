import React from 'react'

export default function RootPage() {
    return (
        <div>
            <div className="flex items-center justify-center bg-gradient-to-r from-blue-900 to-fuchsia-700 text-center">
                <div className="flex-col items-center justify-center text-white">
                    <div className=" w-[70%] mx-auto  md:w-1/2 flex items-center justify-center  text-3xl lg:text-5xl font-bold mx-14 py-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita voluptates aliquam officiis, mollitia quaerat perferendis? Ipsam exercitationem nostrum et repellat?</div>
                    <div className="flex">
                        <div className="flex md:flex-row flex-col p-4 space-x-4 space-y-2 max-w-7xl justify-around w-full h-auto lg:h-60 items-center mx-auto">
                            <div className="h-40 w-[70%] md:w-1/4 flex items-center justify-center ml-4">
                                <div className="flex-col space-y-2 items-center px-0 md:px-6">
                                    <div className="text-sm font-medium text-gray-200">Clients</div>
                                    <div className="text-5xl font-bold">15k+</div>
                                    <div className="text-sm font-medium text-gray-200">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ducimus, quae!</div>
                                </div>
                            </div>
                            <div className="h-40 w-[70%] md:w-1/4 flex items-center justify-center">
                                <div className="flex-col space-y-2">
                                    <div className="text-sm font-medium text-gray-200">Users</div>
                                    <div className="text-5xl font-bold">1.2M+</div>
                                    <div className="text-sm font-medium text-gray-200">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla, debitis.</div>
                                </div>
                            </div>
                            <div className="h-40 w-[70%] md:w-1/4 flex items-center justify-center">
                                <div className="flex-col space-y-2">
                                    <div className="text-sm font-medium text-gray-200">Engagement</div>
                                    <div className="text-5xl font-bold">69k</div>
                                    <div className="text-sm font-medium text-gray-200">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex adipisci minus placeat facere?</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <script src="https://cdn.tailwindcss.com"></script>

        </div>
    )
}
