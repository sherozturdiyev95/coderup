import { BrowserRouter, Route, Routes } from "react-router-dom"
import Login from "./auth/Login"
import RootPage from "./pages/RootPage"
import { useState } from "react"


function App() {
  const [isLogin, setIsLogin] = useState(false)
  return (
    <>
      <BrowserRouter>
        <Routes>
          {
            isLogin ?
              <>
                <Route path="/" element={<RootPage />}></Route>
              </>
              :
              <>
                <Route path="/login" element={<Login isLogin={isLogin} setIsLogin={setIsLogin} />}></Route>
                <Route path="/" element={<Login isLogin={isLogin} setIsLogin={setIsLogin} />}></Route>
              </>
          }
        </Routes>
      </BrowserRouter>
      {/* <Login /> */}
      {/* <RootPage/> */}
    </>
  )
}

export default App
