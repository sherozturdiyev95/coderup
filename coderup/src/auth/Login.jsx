import { notification } from 'antd';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';


export default function Login({ isLogin, setIsLogin }) {
    const [username, setusername] = useState('');
    const [pass, setpass] = useState('');
    const navigate = useNavigate()

    const login__handle = () => {
        if (username === "Oybek" && pass === "Oybek") {
            setIsLogin(true)
            navigate('/')
            notification.success({ message: "Welcome!" })
        } else {
            notification.error({ message: "Username or password is incorrect" })
        }
    }
    return (
        <div>
            <div className="flex flex-col justify-center items-center h-screen bg-gradient-to-t from-black via-purple-900 to-violet-600">
                <div className=" md:w-auto w-[90%] p-8 rounded-xl  m-4 flex flex-col items-center shadow-lg border border-gray-400 opacity-90 ">
                    <div className="text-xl cursor-pointer flex flex-col justify-center items-center mt-5 md:mt-0 ">
                        <h1 className="font-semibold text-3xl text-gray-200 m-2">Log In</h1>
                    </div>
                    <div className="flex flex-col justify-center items-center mt-10 md:mt-4 space-y-6 md:space-y-8">
                        <div className="">
                            <div className="m-1 text-lg text-gray-200 text-semibold">Username</div>
                            <input type="text"
                                className="border-b border-gray-200 focus:outline-none  text-gray-200 placeholder:opacity-50 font-semibold md:w-72 lg:w-[340px] bg-transparent"
                                onInput={(e) => { setusername(e.target.value) }}
                            />
                        </div>
                        <div className="">
                            <div className="m-1 text-lg text-gray-200 text-semibold">Password</div>
                            <input type="password"
                                className="border-b border-gray-200 focus:outline-none  text-gray-200 placeholder:opacity-50 font-semibold md:w-72 lg:w-[340px] bg-transparent"
                                onInput={(e) => { setpass(e.target.value) }}
                            />
                        </div>

                    </div>
                    <div className="text-center mt-7">
                        <button
                            className="uppercase px-24 md:px-[118px] lg:px-[140px] py-2 rounded-md text-white bg-gradient-to-t from-stone-900 via-purple-900 to-violet-600  font-medium active:bg-violet-500"
                            onClick={login__handle}
                        >login</button>
                    </div>
                </div>
                <div className="text-center my-6 flex flex-col">
                    <a href="#" className="text-sm font-medium text-gray-400 hover:text-violet-500 m-1">Forgot
                        Password ?</a>
                    <a href="#" className="text-sm font-bold text-gray-400 hover:text-violet-500 m-1">
                        Not a User? Create New Account</a>
                </div>

            </div>
        </div>
    )
}
